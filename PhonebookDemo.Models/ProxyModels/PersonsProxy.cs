﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonebookDemo.Models.ProxyModels
{
    public class PersonsProxy
    {
        public IEnumerable<Persons> Data;
        public int TotalCount;
    }
}
