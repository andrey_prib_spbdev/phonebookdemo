﻿using PhonebookDemo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using PhonebookDemo.Models;

namespace PhonebookDemo.DAL
{
    public class PersonRepository : IPersonRepository
    {
        private AddressBookDemoEntities ctx = new AddressBookDemoEntities();

        public void AddPerson(Persons person)
        {
            ctx.Persons.Add(person);
            ctx.SaveChanges();
        }

        public void UpdatePerson(Persons person)
        {
            ctx.Entry(person).State = EntityState.Modified;
            ctx.SaveChanges();
        }

        public void DeletePerson(int Id)
        {
            var person = ctx.Persons.Find(Id);
            ctx.Persons.Remove(person);
            ctx.SaveChanges();
        }

        public Persons GetPerson(int Id)
        {
            var person = ctx.Persons.Find(Id);
            return person;
        }

        public IEnumerable<Persons> GetPersons()
        {
            return ctx.Persons;
        }
    }
}
