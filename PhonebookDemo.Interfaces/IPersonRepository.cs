﻿using PhonebookDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonebookDemo.Interfaces
{
    public interface IPersonRepository
    {
        IEnumerable<Persons> GetPersons();

        void AddPerson(Persons person);

        void DeletePerson(int Id);

        void UpdatePerson(Persons person);

        Persons GetPerson(int Id);
    }
}
