Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.panel.*',
    'Ext.layout.container.Border'
]);

Ext.onReady(function () {
    var editwindow = null;

    var form = Ext.widget('form', {
        bodyStyle: 'padding: 5px;',
        items: [{
            xtype: 'hiddenfield',
            name: 'Id'
        },
        {
            xtype: 'textfield',
            name: 'Firstname',
            fieldLabel: 'Firstname',
            allowBlank: false,
            minLength: 2
        }, {
            xtype: 'textfield',
            name: 'Lastname',
            fieldLabel: 'Lastname',
            allowBlank: false,
            minLength: 2
        },
        {
            xtype: 'textfield',
            name: 'Phone',
            fieldLabel: 'Phone',
            allowBlank: false,
            regex: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/
        },
        {
            xtype: 'textfield',
            name: 'Address',
            fieldLabel: 'Address',
            minLength: 2,
            allowBlank: false
        },
        {
            xtype: 'textfield',
            name: 'Email',
            fieldLabel: 'Email Address',
            vtype: 'email'  // requires value to be a valid email address format
        },
        {
            xtype: 'button',
            name: 'save',
            text: 'Save',
            itemId: 'save',
            formBind: true,
            iconCls: 'save',
            handler: addPerson
        },
        {
            xtype: 'button',
            text: 'Cancel',
            itemId: 'cancel',
            iconCls: 'cancel',
            handler: function (btn) {
                var win = btn.up('window').hide();
            }
        }
        ]
    })

    //person data window
    Ext.define('EditWindow', {
        extend: 'Ext.window.Window',
        title: 'Person Data',
        height: 250,
        bodyStyle: 'padding: 10px;',
        width: 300,
        resizable: false,
        items: form
    });

    //person class
    Ext.define('Person', {
        extend: 'Ext.data.Model',
        idProperty: 'Id',
        fields: [
            'Id',
            'Lastname',
            'Firstname',
            'Phone',
            'Address',
            'Email'
        ]
    });

    // create the Data Store
    var store = Ext.create('Ext.data.Store', {
        model: 'Person',
        autosync: true,
        buffered: true,
        leadingBufferZone: 10,
        pageSize: 10,
        enablePaging: true,
        proxy: {
            type: 'ajax',
            url: '/Home/GetPersons',
            reader: {
                root: 'Data',
                totalProperty: 'TotalCount',
                idProperty: 'Id'
            },
            api: {
                create: '/Home/AddPerson',
                read: '/Home/GetPersons',
                update: '/Home/UpdatePerson',
                destroy: '/Home/DeletePerson'
            }
        }
    });

    var grid = Ext.create('Ext.grid.Panel', {
        width: 700,
        height: 500,
        title: 'Phonebook',
        store: store,
        // grid columns
        columns: [
            { xtype: 'rownumberer', width: 30, sortable: false },
            { text: "Lastname", width: 120, dataIndex: 'Lastname', sortable: true },
            { text: "Firstname", flex: 1, dataIndex: 'Lastname', sortable: true },
            { text: "Address", width: 125, dataIndex: 'Address', sortable: true },
            { text: "Phone", width: 125, dataIndex: 'Phone', sortable: true },
            { text: "Email", width: 125, dataIndex: 'Email', sortable: true }
        ],
        tbar: [{
            text: 'Add Person',
            iconCls: 'person-add',
            handler: function () {
                if (!editwindow) {
                    editwindow = new EditWindow;
                }
                editwindow.show();
                editwindow.down('form').getForm().reset();
                editwindow.down('form').getComponent('save').setHandler(addPerson);
            }
        },
        {
            itemId: 'editPerson',
            text: 'Edit Person',
            iconCls: 'person-edit',
            handler: function () {
                if (!editwindow) {
                    editwindow = new EditWindow;
                }
                editwindow.show();
                var person = grid.getSelectionModel().getSelected().items[0].data;
                editwindow.down('form').getForm().setValues(
                    person
                );
                editwindow.down('form').getComponent('save').setHandler(updatePerson);
            },
            disabled: true
        }, {
            itemId: 'removePerson',
            text: 'Remove Person',
            iconCls: 'person-remove',
            handler: function () {
                var personId = grid.getSelectionModel().getSelected().items[0].id;
                Ext.Ajax.request({
                    url: '/Home/DeletePerson',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    method: 'POST',
                    params: Ext.util.JSON.encode({ Id: personId }),
                    success: function (transport) {
                        store.load();
                    },
                    failure: function (transport) {
                        console.log("Error: " - transport.responseText);
                    }
                });
                store.load();
            },
            disabled: true
        }
        ],
        renderTo: 'addressGrid'
    });

    grid.getSelectionModel().on('selectionchange', function (sm, selectedRecord) {
        grid.down('#removePerson').setDisabled(!selectedRecord.length);
        grid.down('#editPerson').setDisabled(!selectedRecord.length);
    });

    function updatePerson() {
        var form = editwindow.down('form');
        var person = form.getForm().getValues();
        Ext.Ajax.request({
            url: '/Home/UpdatePerson',
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            params: Ext.util.JSON.encode(person),
            success: function (transport) {
                store.load();
                editwindow.hide();
            },
            failure: function (transport) {
                console.log("Error: " - transport.responseText);
            }
        });
    }

    function addPerson() {
        var form = editwindow.down('form');
        var person = form.getForm().getValues();
        Ext.Ajax.request({
            url: '/Home/AddPerson',
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST',
            params: Ext.util.JSON.encode(person),
            success: function (transport) {
                store.load();
                editwindow.hide();
            },
            failure: function (transport) {
                console.log("Error: " - transport.responseText);
            }
        });
    }

    store.load();
});

