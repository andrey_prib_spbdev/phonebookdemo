﻿using PhonebookDemo.DAL;
using PhonebookDemo.Interfaces;
using PhonebookDemo.Models;
using PhonebookDemo.Models.ProxyModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhonebookDemo.Controllers
{
    public class HomeController : Controller
    {
        private IPersonRepository _personsRepository = new PersonRepository();

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetPersons(int page = 1, int start = 0, int limit = 100)
        {
            var result = new PersonsProxy()
            {
                Data = _personsRepository.GetPersons().Skip(start).Take(limit),
                TotalCount = _personsRepository.GetPersons().Count()
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void DeletePerson(int Id)
        {
            _personsRepository.DeletePerson(Id);

        }


        [HttpPost]
        public void AddPerson(Persons person)
        {
            _personsRepository.AddPerson(person);
        }

        [HttpPost]
        public void UpdatePerson(Persons person)
        {
            _personsRepository.UpdatePerson(person);
        }
    }
}